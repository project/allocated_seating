<?php
/**
 * @file
 * Provides node hooks for allocated_seating_example module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands group dot com
 */

/***********************************************
 * General module node functions
 **********************************************/
/**
 * Implementation of hook_access().
 */
function allocated_seating_example_access($op, $node, $account) {
  global $user;

  $type = node_get_types('type', $node);
  if ($op == 'create') {
    return user_access('create ' . $type->name . ' content');
  }

  if ($op == 'update') {
    if ((user_access('edit own ' . $type->name . ' content') && ($user->uid == $node->uid))
        || user_access('edit any ' . $type->name . ' content')) {
      return TRUE;
    }
  }
  if ($op == 'delete') {
    if ((user_access('delete own ' . $type->name . ' content') && ($user->uid == $node->uid))
        || user_access('delete any ' . $type->name . ' content')) {
      return TRUE;
    }
  }
}

/**
 * Implementation of hook_form().
 */
function allocated_seating_example_form(&$node, $form_state) {
  // The site admin can decide if this node type has a title and body, and how
  // the fields should be labeled. We need to load these settings so we can
  // build the node form correctly.
  $type = node_get_types('type', $node);

  $form['basic'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#title'        => t('Basic Details'),
    '#collapsed'    => FALSE,
    '#weight'       => -12,
    '#group'        => 'allocated_seating_example',
  );

  if ($type->has_title) {
    $form['basic']['title'] = array(
      '#type' => 'textfield',
      '#title' => check_plain($type->title_label),
      '#required' => TRUE,
      '#default_value' => isset($form_state['values']['title']) ? $form_state['title'] : $node->title,
      '#weight' => -5
    );
  }

  if ($type->has_body) {
    // In Drupal 6, we can use node_body_field() to get the body and filter
    // elements. This replaces the old textarea + filter_form() method of
    // setting this up. It will also ensure the teaser splitter gets set up
    // properly.
    $form['basic']['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
  }

  $type_name = $node->type;
  allocated_seating_example_inc('util');
  $function = 'allocated_seating_example_node_' . $type_name . '_form';
  if (function_exists($function)) {
    $function($form, $node, $form_state);
  }


  if (isset($node->nid)) {
    $form['#action'] = url('node/' . $node->nid . '/edit');
  }
  else {
    $form['#action'] = url('node/add/' . str_replace('_', '-', $node->type));
  }
  return $form;
}


/**
 * Implementation of hook_validate().
 */
function allocated_seating_example_validate($node, &$form) {
  $type = $node->type;
  allocated_seating_example_inc('util');
  $function = 'allocated_seating_example_node_' . $type . '_validate';
  if (function_exists($function)) {
    $function($node, $form);
  }
}


/**
 * Implementation of hook_insert().
 */
function allocated_seating_example_insert($node) {
  $type = $node->type;
  allocated_seating_example_inc('util');
  $function = 'allocated_seating_example_node_' . $type . '_insert';
  if (function_exists($function)) {
    $function($node);
  }
}


/**
 * Implementation of hook_update().
 */
function allocated_seating_example_update($node) {
  // If this is a new node or we're adding a new revision.
  if ($node->revision) {
    allocated_seating_example_insert($node);
  }
  else {
    $type = $node->type;
    allocated_seating_example_inc('util');
    $function = 'allocated_seating_example_node_' . $type . '_update';
    if (function_exists($function)) {
      $function($node);
    }
  }
}


/**
 * Implementation of hook_delete().
 */
function allocated_seating_example_delete(&$node) {
  $type = $node->type;
  allocated_seating_example_inc('util');
  $function = 'allocated_seating_example_node_' . $type . '_delete';
  if (function_exists($function)) {
    $function($node);
  }
}


/**
 * Implementation of hook_load().
 */
function allocated_seating_example_load($node) {
  $type = $node->type;
  allocated_seating_example_inc('util');
  $function = 'allocated_seating_example_node_' . $type . '_load';
  if (function_exists($function)) {
    $return = $function($node);
  }
  return $return;
}


/**
 * Implementation of hook_view().
 */
function allocated_seating_example_view($node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);
  $type = $node->type;
  allocated_seating_example_inc('util');
  $function = 'allocated_seating_example_node_' . $type . '_view';
  if (function_exists($function)) {
    $function($node, $teaser, $page);
  }

  return $node;
}

/***********************************************
 * Ticket layout node type handlers
 **********************************************/
/**
 * Hook form handler
 */
function allocated_seating_example_node_ticket_layout_form(&$form, &$node, $form_state) {
  // Add to the form array.
  $form['ticketing'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#title'        => t('Ticketing Details'),
    '#collapsed'    => FALSE,
    '#weight'       => -10,
    '#group'        => 'allocated_seating_example',
  );

  $form['ticketing']['number_of_tickets'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Number of tickets'),
    '#default_value' => $form_state['values']['number_of_tickets'] ? $form_state['values']['number_of_tickets'] : $node->number_of_tickets,
    '#size'          => 60,
    '#description'   => t('Enter the number of tickets available for sale in this ticketing layout'),
    '#maxlength'     => 10,
    '#required'      => TRUE,
  );

  allocated_seating_example_inc('util');

  $allocated_ticketing_default = (isset($form_state['values']['allocated_ticketing']) ? $form_state['values']['allocated_ticketing']
                          : (isset($node->allocated_ticketing) ? $node->allocated_ticketing : 0));

  $form['ticketing']['allocated_ticketing'] = array(
    '#type'          => 'radios',
    '#default_value' => $allocated_ticketing_default,
    '#title'       => t('Allocated Ticketing?'),
    '#description' => t('Choose the ticketing arrangement for this ticket layout'),
    '#options'    => array(
      1 => 'Yes',
      0 => 'No'
    ),
    '#ahah' => array(
      'path' => 'allocated_seating_example/ahah/ticketing/ticket_plan',
      'wrapper' => 'ticket-layout-plan-wrapper',
      'event' => 'change',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  if ($allocated_ticketing_default) {
    // Don't need this without allocated ticketing.
    $form['ticketing']['number_of_tickets']['#ahah'] = array(
      'path' => 'allocated_seating_example/ahah/ticketing/ticket_plan',
      'wrapper' => 'ticket-layout-plan-wrapper',
      'event' => 'change',
      'method' => 'replace',
      'effect' => 'fade',
    );
  }
  $form['#cache'] = TRUE;

  $tickets_default = (isset($form_state['values']['number_of_tickets']) ? $form_state['values']['number_of_tickets'] : $node->number_of_tickets);
  if ($tickets_default && $allocated_ticketing_default) {
    $form['ticketing']['ticket_plan'] = array(
      '#prefix' => '<div class="clear-block" id="ticket-layout-plan-wrapper">',
      '#suffix' => '</div>',
      '#description' => t('Enter the number of rows and columns in your layout. You may drag and drop seats to reorder them. You
                          may add another floor/section/level by clicking add floor. You can drag seats from one floor to another
                          by dropping them on the tab of the floor you wish to drag them to. '),
      '#ahah_extra' => 'ticketing/',
      '#default_value' => (isset($form_state['values']['ticket_plan']) ? $form_state['values']['ticket_plan'] : $node->ticket_plan),
      '#type' => 'allocated_seating_seat_design',
      '#seats' => $tickets_default
    );
  }
  else {
    // Add these here in case ahah adds our plan.
    allocated_seating_setup();
    $form['ticketing']['ticket_plan'] = array(
      '#prefix' => '<div class="clear-block" id="ticket-layout-plan-wrapper">',
      '#suffix' => '</div>',
      '#value' => t('This ticket layout method does not have allocated ticketing or the number of tickets is not known')
    );
  }
}

/**
 * Hook validate handler
 */
function allocated_seating_example_node_ticket_layout_validate($node, &$form) {
  // Test the form.
  if ($node->allocated_ticketing && !$node->ticket_plan) {
    form_set_error('ticket_plan', t('You must provide a ticketing plan'));
  }
}

/**
 * Hook insert handler
 */
function allocated_seating_example_node_ticket_layout_insert($node) {
  // Insert into db.
  $node->ticket_plan = serialize($node->ticket_plan);
  $result = drupal_write_record('allocated_seating_example_ticket_layouts', $node);
}

/**
 * Hook update handler
 */
function allocated_seating_example_node_ticket_layout_update($node) {
  // Do the update.
  $update = array('vid');
  $node->ticket_plan = serialize($node->ticket_plan);
  $result = drupal_write_record('allocated_seating_example_ticket_layouts', $node, $update);
}

/**
 * Hook delete handler
 */
function allocated_seating_example_node_ticket_layout_delete(&$node) {
  // Do the delete.
  db_query("DELETE FROM {allocated_seating_example_ticket_layouts} WHERE vid = %d", $node->vid);
}

/**
 * Hook load handler
 */
function allocated_seating_example_node_ticket_layout_load($node) {
  $data = db_fetch_object(db_query("SELECT * FROM {allocated_seating_example_ticket_layouts}
                                   WHERE vid = %d", $node->vid));
  $data->ticket_plan = unserialize($data->ticket_plan);
  return $data;
}
