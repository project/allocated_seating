<?php
/*
 * @file allocated_seating_example.util.inc
 * Provides utility functions for allocated_seating_example module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 *
 */

/**
 * Utility to test if function is a allocated_seating_example node type
 * @param $type string node type machine name
 * @return bool
*/
function allocated_seating_example_is_allocated_seating_example($type) {
  $types = array_keys(allocated_seating_example_node_info());
  return (in_array($type, $types) &&
          !in_array($type, array('ticket_layout', 'allocated_seating_example_feed_csv', 'allocated_seating_example_feed_json')));
}
